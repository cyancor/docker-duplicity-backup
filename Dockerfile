FROM alpine:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

# Taken from https://github.com/lagun4ik/docker-backup/blob/master/Dockerfile
RUN apk add --update \
    git \
    python \
    py-pip \
    py-cffi \
    py-cryptography \
    py-openssl \
    py-boto \
    mailx \
    bash \
    duplicity \
    ca-certificates \
	make \
  && pip install --upgrade pip \
  && apk add --virtual build-deps \
    gcc \
    libffi-dev \
    python-dev \
    linux-headers \
    musl-dev \
    openssl-dev \
  && pip install gsutil \
  && pip install fasteners \
  && pip install paramiko \
  && pip install s3cmd \
  && apk del build-deps \
  && rm -rf /var/cache/apk/*	
  
RUN git clone https://github.com/zertrin/duplicity-backup.sh.git /duplicity-backup
RUN chmod 0777 /duplicity-backup/duplicity-backup.sh

RUN mkdir /artifacts

COPY duplicity-backup.conf /duplicity-backup/duplicity-backup.conf
COPY add-target.sh /add-target.sh
COPY start.sh /start.sh
RUN chmod 0777 /add-target.sh
RUN chmod 0777 /start.sh
